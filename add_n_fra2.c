#include<stdio.h>
struct frac
{
    int n,d;
};
typedef struct frac frac;
int input_n()
{
    int n;
    printf("Enter the number of fractions\n");
    scanf("%d",&n);
    return n;
}
void input_f(int n,frac f[n])
{
	int i;
    for(i=0;i<n;i++)
    {
        printf("Fraction %d.\nEnter the numerator and denominator of fraction\n",i+1);
        scanf("%d %d",&f[i].n,&f[i].d);
    }
}
int find_gcd(int a,int b)
{
	int i,max,gcd;
	max=a>b?a:b;
	for(i=max;i>1;i--)
	{
		if(a%i==0&&b%i==0)
		{
			gcd=i;
			break;
		}
	}
	return gcd;
}
frac find_sum(int n,frac f[n])
{
    frac sum;
	int i,gcd;
    sum.d=1;
	sum.n=0;
	for(i=0;i<n;i++)
	{
		sum.d=sum.d*f[i].d;
	}
	for(i=0;i<n;i++)
	{
		sum.n=sum.n+(f[i].n*sum.d)/f[i].d;
	}
	gcd=find_gcd(sum.n,sum.d);
	sum.n=sum.n/gcd;
	sum.d=sum.d/gcd;
	return sum;
}
void output(int n,frac sum)
{
    printf("Sum of %d fractions is %d/%d\n",n,sum.n,sum.d);
}
int main()
{
    frac f[100],sum;
    int n;
    n=input_n();
    input_f(n,f);
    sum=find_sum(n,f);
    output(n,sum);
    return 0;
}