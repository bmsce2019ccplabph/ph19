#include<stdio.h>
struct frac
{
    int n,d;
};
typedef struct frac frac;
int input_n()
{
    int n;
    printf("Enter the number of fractions\n");
    scanf("%d",&n);
    return n;
}
void input_f(int n,frac f[n])
{
	int i;
    for(i=0;i<n;i++)
    {
        printf("Fraction %d.\nEnter the numerator and denominator of fraction\n",i+1);
        scanf("%d %d",&f[i].n,&f[i].d);
    }
}
void get_deno(int n,frac f[n],int d[n])
{
	int i;
	for(i=0;i<n;i++)
	{
		d[i]=f[i].d;
	}
}
int find_lcm(int n,int d[n])
{
    int lcm=1,l,i,j,flag=0;
    l=d[0];
    for(i=1;i<n;i++)
    {
        if(l<d[i])
            l=d[i];
    }
    for(i=2;i<=l;i++)
    {
        for(j=0;j<n;j++)
        {
            if(d[j]%i==0)
            {
                flag=1;
				d[j]=d[j]/i;
            }
        }
		if(flag==1)
		{
			lcm=lcm*i;
		}
    }
    return lcm;
}
frac find_sum(int n,frac f[n])
{
    frac sum;
    int num=0,deno,i,min,d[100];
	get_deno(n,f,d);
    deno=find_lcm(n,d);
    for(i=0;i<n;i++)
    {
        num=num+((f[i].n*deno)/f[i].d);
    }
    min=num<deno?num:deno;
    for(i=min;i>=2;i--)
    {
        if(num%i==0&&deno%i==0)
        {
            num=num/i;
            deno=deno/i;
        }
    }
    sum.n=num;
    sum.d=deno;
    return sum;
}
void output(int n,frac sum)
{
    printf("Sum of %d fractions is %d/%d\n",n,sum.n,sum.d);
}
int main()
{
    frac f[100],sum;
    int n;
    n=input_n();
    input_f(n,f);
    sum=find_sum(n,f);
    output(n,sum);
    return 0;
}