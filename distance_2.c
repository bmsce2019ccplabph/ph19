#include<stdio.h>
#include<math.h> 
void input(float *,float *);
float find_dist(float,float,float,float);
void output(float);
int main()
{
	float x1,x2,y1,y2;
	float d;
	input(&x1,&y1);
	input(&x2,&y2);
	d=find_dist(x1,y1,x2,y2);
	output(d);
	return 0;
}
void input(float *m,float *n)
{
	printf("Enter the co-ordinates of point\n");
	scanf("%f %f",m,n);
}
float find_dist(float x1,float y1,float x2,float y2)
{
	float dist;
	dist=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
	return dist;
}
void output(float dis)
{
	printf("Distace between the points is %f\n",dis);
}