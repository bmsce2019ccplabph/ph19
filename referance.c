#include<stdio.h>
struct point
{
    float x,y;
};
typedef struct point point;
void input(point *m,point *n)
{
    printf("Enter the co-ordinates of first point\n");
    scanf("%f %f",&m->x,&m->y);
    printf("Enter the co-ordinates of second point\n");
    scanf("%f %f",&n->x,&n->y);
}
int main()
{
    point a,b;
    input(&a,&b);
    printf("Point 1: %f %f\npoint 2: %f %f\n",a.x,a.y,b.x,b.y);
    return 0;
}