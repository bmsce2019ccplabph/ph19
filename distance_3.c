#include<stdio.h>
#include<math.h>
struct point
{
	float x,y;
};
typedef struct point point; 
void input(point *,point *);
float find_dist(point,point);
void output(float);
int main()
{
	point a,b;
	float d;
	input(&a,&b);
	d=find_dist(a,b);
	output(d);
	return 0;
}
void input(point *m,point *n)
{
	printf("Enter the co-ordinates of first point\n");
	scanf("%f %f",&m->x,&m->y);
	printf("Enter the co-ordinates of second point\n");
	scanf("%f %f",&n->x,&n->y);
}
float find_dist(point p,point q)
{
	float dist;
	dist=sqrt(pow((q.x-p.x),2)+pow((q.y-p.y),2));
	return dist;
}
void output(float dis)
{
	printf("Distace between the points is %f\n",dis);
}