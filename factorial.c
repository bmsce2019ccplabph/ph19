#include<stdio.h>
int fact(int);
int main()
{
    int num,res;
    printf("Enter the number to calculate it's factorial:");
    scanf("%d",&num);
    res=fact(num);
    printf("\nFactorial of given number is %d\n",res);
    return 0;
}
int fact(int n)
{
    int f=1,i;
    for(i=1;i<=n;i++)
    {
        f=f*i;
    }
    return f;
}