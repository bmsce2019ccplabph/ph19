#include<stdio.h>
#include<math.h>
struct vertex
{
    float x,y;
};
typedef struct vertex vertex;
int input_n()
{
	int n;
	printf("Enter the number of polygons\n");
	scanf("%d",&n);
	return n;
}
void input(int n,int m[n],vertex fp[50][50])
{
    int i,j;
	for(i=0;i<n;i++)
	{
		printf("Enter the number of vertices\n");
		scanf("%d",&m[i]);
        printf("Enter the vertices\n");
        for(j=0;j<m[i];j++)
        {
            scanf("%f %f",&fp[i][j].x,&fp[i][j].y);
            printf("\n");
        }
    }
}
float find_distance(vertex v1,vertex v2)
{
    float d;
    d=sqrt(pow((v1.x-v2.x),2)+pow((v1.y-v2.y),2));
    return d;
}
float area_triangle(float s,float a,float b,float c)
{
    float area;
    area=sqrt(s*(s-a)*(s-b)*(s-c));
    return area;
}
void find_area(int n,int m[n],vertex fp[n][50],float area[50])
{
    int i,j;
    float s,a,b,c,area_t;
    for(i=0;i<n;i++)
    {
        area[i]=0;
        c=find_distance(fp[i][0],fp[i][1]);
        for(j=0;j<m[i]-2;j++)
        {
            a=c;
            b=find_distance(fp[i][j+1],fp[i][j+2]);
            c=find_distance(fp[i][0],fp[i][j+2]);
            s=(a+b+c)/2;
            area_t=area_triangle(s,a,b,c);
            area[i]=area[i]+area_t;
        }
    }
}
void output(int n,float area[n])
{
    int i;
    for(i=0;i<n;i++)
    {
     printf("%f\n",area[i]);
    }
}
int main()
{
    int n,m[50];
	float area[50];
    vertex p[50][50];
	n=input_n();
    input(n,m,p);
    find_area(n,m,p,area);
    output(n,area);
    return 0;
}