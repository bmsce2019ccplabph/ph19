#include<stdio.h>
int input_n();
void input(int,int *);
int find_sum(int,int *);
void output(int);
int main()
{
    int n,a[100],sum;
    n=input_n();
    input(n,a);
    sum=find_sum(n,a);
    output(sum);
    return 0;
}
int input_n()
{
    int n;
    printf("Enter the value of n\n");
    scanf("%d",&n);
    return n;
}
void input(int n,int arr[n])
{
    int i;
    printf("Enter the numbers\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
    }
}
int find_sum(int n,int arr[n])
{
    int sum=0,i;
    for(i=0;i<n;i++)
    {
        sum=sum+arr[i];
    }
    return sum;
}
void output(int sum)
{
    printf("Sum is %d\n",sum);
}