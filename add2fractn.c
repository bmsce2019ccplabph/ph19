#include<stdio.h>
struct frac
{
    int n,d;
};
typedef struct frac frac;
void input(frac *a,frac *b)
{
    printf("Enter the numerator and denominator of fraction 1\n");
    scanf("%d %d",&a->n,&a->d);
    printf("Enter the numerator and denominator of fraction 2\n");
    scanf("%d %d",&b->n,&b->d);
}
frac find_sum(frac f1,frac f2)
{
    frac sum;
    int num,deno,i,min;
    deno=f1.d*f2.d;
    num=(f1.n*f2.d)+(f2.n*f1.d);
    min=num<deno?num:deno;
    for(i=min;i>=2;i--)
    {
        if(num%i==0&&deno%i==0)
        {
            num=num/i;
            deno=deno/i;
        }
    }
    sum.n=num;
    sum.d=deno;
    return sum;
}
void output(frac sum)
{
    printf("Sum of two fractions is %d/%d\n",sum.n,sum.d);
}
int main()
{
    frac f1,f2,sum;
    input(&f1,&f2);
    sum=find_sum(f1,f2);
    output(sum);
    return 0;
}
    