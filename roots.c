
#include<stdio.h>
#include<math.h>
int main()
{
    int a,b,c,deno;
    float root1,root2,d;
    printf("Enter the co-efficients\n");
    scanf("%d %d %d",&a,&b,&c);
    d=b*b-4*a*c;
    deno=2*a;
    if(a!=0)
    {
        if(d==0)
        {
            printf("Roots are equal\n");
            root1=-b/deno;
            printf("Roots are %f and %f\n",root1,root1);
        }
        else if(d>0)
        {
            root1=(-b+sqrt(d))/deno;
            root2=(-b-sqrt(d))/deno;
            printf("Roots are real and distinct.\nRoots are %f and %f\n",root1,root2);
        }
        else
        {
            printf("Roots are imaginary\n");
        }
    }
    else
    {
        printf("It is not a quadratic equation\n");
    }
    return 0;
} 